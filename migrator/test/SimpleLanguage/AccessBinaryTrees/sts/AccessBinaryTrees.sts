/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Isaac Gouy
*/
export {}

function AccessBinaryTrees() {}

function TreeNode(left, right, item) {
  this.left = left;
  this.right = right;
  this.item = item;
}

TreeNode.prototype.itemCheck = function() {
  if (this.left == null)
    return this.item;
  else
    return this.item + this.left.itemCheck() - this.right.itemCheck();
};

AccessBinaryTrees.prototype.bottomUpTree = function(item, depth) {
  if (depth > 0) {
    return new TreeNode(
        this.bottomUpTree(2 * item - 1, depth - 1),
        this.bottomUpTree(2 * item, depth - 1), item);
  } else {
    return new TreeNode(null, null, item);
  }
};

AccessBinaryTrees.prototype.setup = function() {};

AccessBinaryTrees.prototype.run = function() {
  var ret = 0;

  var n1 = 4;
  var n2 = 7;
  var expected = -4;

  for (var n = n1; n <= n2; n += 1) {
    var minDepth = n1;
    var maxDepth = Math.max(minDepth + 2, n);
    var stretchDepth = maxDepth + 1;

    var check = this.bottomUpTree(0, stretchDepth).itemCheck();

    var longLivedTree = this.bottomUpTree(0, maxDepth);
    for (var depth = minDepth; depth <= maxDepth; depth += 2) {
      var iterations = 1 << (maxDepth - depth + minDepth);

      check = 0;
      for (var i = 1; i <= iterations; i++) {
        check += this.bottomUpTree(i, depth).itemCheck();
        check += this.bottomUpTree(-i, depth).itemCheck();
      }
    }

    ret += longLivedTree.itemCheck();
  }

  if (ret != expected)
    throw 'ERROR: bad result: expected ' + expected + ' but got ' + ret;

  var consumer = new consumer();
  consumer.consumeInt(ret);
};

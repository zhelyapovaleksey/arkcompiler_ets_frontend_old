/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// The Computer Language Shootout
// http://shootout.alioth.debian.org/
// contributed by Isaac Gouy

function MathPartialSums() {
  this.flag = 0;
}

MathPartialSums.prototype.partial = function(n) {
  let a1: double = 0.0;
  let a2: double = 0.0;
  let a3: double = 0.0;
  let a4: double = 0.0;
  let a5: double = 0.0;
  let a6: double = 0.0;
  let a7: double = 0.0;
  let a8: double = 0.0;
  let a9: double = 0.0;

  let twothirds: double = 2.0 / 3.0;
  let alt: double = -1.0;
  let k2: double = 0.0;
  let k3: double = 0.0;
  let sk: double = 0.0;
  let ck: double = 0.0;

  for (let k: int = 1; k <= n; k++) {
    k2 = k * k;
    k3 = k2 * k;
    sk = Math.sin(k);
    ck = Math.cos(k);
    alt = -alt;

    a1 += Math.pow(twothirds, k - 1);
    a2 += Math.pow(k, -0.5);
    a3 += 1.0 / (k * (k + 1.0));
    a4 += 1.0 / (k3 * sk * sk);
    a5 += 1.0 / (k3 * ck * ck);
    a6 += 1.0 / k;
    a7 += 1.0 / k2;
    a8 += alt / k;
    a9 += alt / (2 * k - 1);
  }

  let res: double = a1 + a2 + a3 + a4 + a5;

  if (res > 0)
    this.flag = 1;
  else
    this.flag = 2;

  // NOTE: We don't try to validate anything from pow(),  sin() or cos()
  // because those aren't well-specified in ECMAScript.
  return a6 + a7 + a8 + a9;
};

MathPartialSums.prototype.setup = function() {};

MathPartialSums.prototype.run = function() {
  let n1: int = 1024;
  let n2: int = 16384;
  let expected: double = 60.08994194659945;

  let total = 0;

  for (let i: int = n1; i <= n2; i *= 2) total += this.partial(i);

  if (total != expected) {
    throw 'ERROR: bad result: expected ' + expected + ' but got ' + total;
  }

  if (this.flag != 1 && this.flag != 2) {
    throw 'ERROR: bad flag value: expected 1 or 2 but got ' + this.flag;
  }

  let consumer = new consumer();
  consumer.consumeFloat(total);
};

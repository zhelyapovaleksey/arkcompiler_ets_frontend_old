# Antlr4 library
ANTLR (ANother Tool for Language Recognition) is a powerful parser generator for reading, 
processing, executing, or translating structured text or binary files. 

**Official website:** https://www.antlr.org/

## Library version
Version 4.9.3 is selected due to requirement for the migrator project to be compatible with 
Java 8. It may be updated in the future.

## License
ANTLR4 is distributed under [BSD 3-clause license](https://www.antlr.org/license.html).

## Artifacts
The library artifact is downloaded from https://github.com/antlr/website-antlr4/blob/gh-pages/download/antlr-4.9.3-complete.jar
